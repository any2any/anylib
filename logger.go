package anylib

import (
	"fmt"
	"os"
	"time"
)

type Logger struct {
	dateFormat string
}

func (l *Logger) SetDateFormat(dateFormat string) {
	l.dateFormat = dateFormat
}

func (l *Logger) Log(msg string) {
	d := time.Now().Format(l.dateFormat)

	fmt.Println(d + " " + msg)
}

func (l *Logger) Die(msg string) {
	l.Log(msg)
	os.Exit(1)
}

func (l *Logger) Must(msg string, err error) {
	if err != nil {
		l.Die(msg + ": " + err.Error())
	}
}
