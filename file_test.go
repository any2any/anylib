package anylib

import (
	"fmt"
	"testing"
)

func TestReadFileInChunks(t *testing.T) {
	ReadFileInChunks("file.go", 3, func(b []byte) {
		fmt.Println(b)
	})
}
