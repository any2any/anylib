package anylib

import (
	"bufio"
	"io"
	"os"
)

func ReadFileInChunks(filename string, chunkSize int, ignoreZeroBytes bool, callback func([]byte)) error {
	file, err := os.Open(filename)
	if err != nil {
		return err
	}
	defer file.Close()

	reader := bufio.NewReader(file)
	part := make([]byte, 1)

	lastPart := make([]byte, 0)

	for {
		if _, err = reader.Read(part); err != nil {
			break
		}

		if ignoreZeroBytes && part[0] == 0 {
			continue
		}

		if len(lastPart) == chunkSize {
			callback(lastPart)
			lastPart = make([]byte, 0)
		} else {
			lastPart = append(lastPart, part[0])
		}
	}

	if err != io.EOF {
		return err
	}

	if len(lastPart) > 0 {
		for len(lastPart) < chunkSize {
			lastPart = append(lastPart, 0)
		}
		callback(lastPart)
	}

	return nil
}
