package anylib

import (
	"flag"
	"fmt"
	"os"
	"path/filepath"
	"strings"
)

type AnyConfig struct {
	InputFile  string
	OutputFile string
	IgnoreZero bool
	Logger     Logger
}

func LoadAnyConfig(name, defaultOutput, outputExtension string) AnyConfig {
	cfg := AnyConfig{
		Logger: Logger{
			dateFormat: "[02.01.2006 - 15:04:05]",
		},
	}

	help := flag.Bool("h", false, "Show help")
	zeroBytes := flag.Bool("n", false, "Ignore zero bytes")
	input := flag.String("i", "", "Input file")
	output := flag.String("o", defaultOutput, "Output file ("+outputExtension+" file)")

	flag.Parse()

	if help != nil && *help {
		fmt.Println("Usage: " + name + " -i [input-file] [optional-params]")
		fmt.Println("")
		fmt.Println("Optional params")
		fmt.Println("-o [output-file]     Define a different output file")
		fmt.Println("-n                   Ignore zero bytes")
		fmt.Println("-h                   Show this help page")

		os.Exit(0)
	}

	cfg.IgnoreZero = zeroBytes != nil && *zeroBytes

	if input != nil {
		absInput, err := filepath.Abs(*input)
		cfg.Logger.Must("Failed to resolve path for input", err)

		if !_fileExists(absInput) {
			cfg.Logger.Die("Input file \"" + absInput + "\" does not exist")
		}

		cfg.InputFile = absInput
	} else {
		cfg.Logger.Die("Invalid input file")
	}

	if output != nil {
		absOutput, err := filepath.Abs(*output)
		cfg.Logger.Must("Failed to resolve path for output", err)

		if !strings.HasSuffix(absOutput, outputExtension) {
			cfg.Logger.Die("Output file must be of type " + outputExtension)
		}

		cfg.OutputFile = absOutput
	} else {
		cfg.OutputFile = defaultOutput
	}

	cfg.Logger.Log("InputFile: " + cfg.InputFile)
	cfg.Logger.Log("OutputFile: " + cfg.OutputFile)

	return cfg
}

func _fileExists(filename string) bool {
	info, err := os.Stat(filename)
	if os.IsNotExist(err) {
		return false
	}
	return !info.IsDir()
}
